package org.gxgow.composerdemo.persist

import io.objectbox.Box
import org.gxgow.composerdemo.models.Person
import java.security.SecureRandom

class DataGeneration {

    companion object {
        fun generateLocalDatabase(boxStore: Box<Person>) {
            if(boxStore.count() > 0) boxStore.removeAll()
            val persons: MutableList<Person> = mutableListOf()

            for(i in 1..100 ) {
                val random = SecureRandom()
                val person = Person(firstName = "Person$i", lastName = "Jensen$i", age = random.nextInt(50)+10, phone = Person.generateRandomPhoneNumber())
                persons.add(person)
            }
            boxStore.put(persons)
        }
    }
}