package org.gxgow.composerdemo.interfaces;

interface OnFABInteractionListener {
    fun onFABInteraction()
    fun hideFAB()
    fun showFAB()
}
