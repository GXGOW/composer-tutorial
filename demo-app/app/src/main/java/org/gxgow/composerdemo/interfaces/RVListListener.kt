package org.gxgow.composerdemo.interfaces;

import org.gxgow.composerdemo.models.Person;

interface RVListListener {
    fun onListInteraction(person: Person);
}
