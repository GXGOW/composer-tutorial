package org.gxgow.composerdemo.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import kotlinx.android.synthetic.main.fragment_person_detail.*
import org.gxgow.composerdemo.App
import org.gxgow.composerdemo.R
import org.gxgow.composerdemo.interfaces.OnFABInteractionListener
import org.gxgow.composerdemo.models.Person
import org.jetbrains.anko.support.v4.toast

class PersonDetailFragment : Fragment() {
    private var listener: OnFABInteractionListener? = null
    private var id: Long? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_person_detail, container, false)
        listener?.hideFAB()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        if (bundle != null) {
            val person = bundle.getSerializable("person") as Person
            id = person.id
            et_firstName.setText(person.firstName)
            et_lastName.setText(person.lastName)
            et_age.setText(person.age.toString())
            et_phoneNumber.setText(person.phone)
        }
        btn_save.setOnClickListener { saveChanges() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFABInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener?.showFAB()
    }

    private fun checkRequiredFields(): Boolean {
        var check = true
        if (textFieldEmpty(et_firstName)) {
            et_firstName.error = getString(R.string.required)
            check = false
        }
        if (textFieldEmpty(et_lastName)) {
            et_lastName.error = getString(R.string.required)
            check = false
        }
        if (textFieldEmpty(et_age)) {
            et_age.error = getString(R.string.required)
            check = false
        }
        if (textFieldEmpty(et_phoneNumber)) {
            et_phoneNumber.error = getString(R.string.required)
            check = false
        }
        return check
    }

    private fun textFieldEmpty(textField: EditText): Boolean {
        val text = textField.text.toString()
        return text.trim() == ""
    }

    private fun saveChanges() {
        if (checkRequiredFields()) {
            val box = (activity!!
                    .application as App)
                    .personBox
            val person: Person
            if (id == null) {
                person = Person(firstName = et_firstName.text.toString(),
                        lastName = et_lastName.text.toString(),
                        age = et_age.text.toString().toInt(),
                        phone = et_phoneNumber.text.toString())
            } else {
                person = box.get(id!!)
                person.firstName = et_firstName.text.toString()
                person.lastName = et_lastName.text.toString()
                person.age = et_age.text.toString().toInt()
                person.phone = et_phoneNumber.text.toString()
            }
            box.put(person)
            toast(getString(R.string.creation_success))
            activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }
}
