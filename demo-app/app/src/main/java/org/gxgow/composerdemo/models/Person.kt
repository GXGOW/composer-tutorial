package org.gxgow.composerdemo.models

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.io.Serializable
import java.security.SecureRandom

@Entity
class Person(@Id var id: Long = 0,
             var firstName: String,
             var lastName: String,
             var age: Int,
             var phone: String): Serializable {
    companion object {
        fun generateRandomPhoneNumber(): String {
            val random = SecureRandom()
            var phone = "04"
            for(i in 0..7) {
                phone ="$phone${random.nextInt(9)}"
            }
            return phone
        }
    }
}
