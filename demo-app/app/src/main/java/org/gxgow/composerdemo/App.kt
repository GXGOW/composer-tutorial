package org.gxgow.composerdemo

import android.app.Application
import io.objectbox.Box
import io.objectbox.BoxStore
import io.objectbox.kotlin.boxFor
import org.gxgow.composerdemo.models.MyObjectBox
import org.gxgow.composerdemo.models.Person
import org.gxgow.composerdemo.persist.DataGeneration

class App : Application() {
    private lateinit var boxStore: BoxStore
    public lateinit var personBox: Box<Person>
        private set

    override fun onCreate() {
        super.onCreate()
        boxStore = MyObjectBox.builder().androidContext(this).build()

        personBox = boxStore.boxFor()
        DataGeneration.generateLocalDatabase(personBox)
    }
}