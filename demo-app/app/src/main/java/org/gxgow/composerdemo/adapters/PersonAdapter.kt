package org.gxgow.composerdemo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.cardview_person.view.*
import org.gxgow.composerdemo.R
import org.gxgow.composerdemo.models.Person
import kotlin.properties.Delegates

class PersonAdapter : RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {

    var persons: MutableList<Person> by Delegates.observable(mutableListOf()) { _, _, _ -> notifyDataSetChanged() }
    var onItemClick: ((Person) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val cardView = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.cardview_person, parent, false)
        return PersonViewHolder(cardView)
    }

    override fun getItemCount(): Int {
        return persons.size
    }

    fun removeItemAt(index: Int): Person {
        return persons.removeAt(index)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.bindView(persons[position])
    }

    inner class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(person: Person) {
            val fullName = "${person.firstName} ${person.lastName}"
            itemView.tv_fullname.text = fullName
            itemView.tv_phoneNumber.text = person.phone
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(persons[adapterPosition])
            }
        }
    }

}