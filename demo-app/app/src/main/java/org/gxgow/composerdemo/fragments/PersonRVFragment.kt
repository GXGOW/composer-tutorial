package org.gxgow.composerdemo.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.objectbox.Box
import jp.wasabeef.recyclerview.animators.LandingAnimator
import kotlinx.android.synthetic.main.fragment_person_rv.*
import org.gxgow.composerdemo.App
import org.gxgow.composerdemo.R
import org.gxgow.composerdemo.adapters.PersonAdapter
import org.gxgow.composerdemo.interfaces.OnFABInteractionListener
import org.gxgow.composerdemo.interfaces.RVListListener
import org.gxgow.composerdemo.models.Person
import org.gxgow.composerdemo.models.Person_


class PersonRVFragment : Fragment() {

    private var fabListener: OnFABInteractionListener? = null
    private var fabBehavior: RecyclerView.OnScrollListener? = null
    private var rvListener: RVListListener? = null
    private var personBox: Box<Person>? = null

    private val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            // Remove given character from the adapter
            val adapter = rv_persons.adapter as PersonAdapter
            val toRemove = viewHolder.adapterPosition
            val person = adapter.removeItemAt(toRemove)
            personBox!!.remove(person)
            // Invoke removal animation
            adapter.notifyItemRemoved(toRemove)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFABInteractionListener)
            fabListener = context
        else
            throw UnsupportedOperationException("OnFABInteractionListener not implemented")
        if (context is RVListListener)
            rvListener = context
        else
            throw UnsupportedOperationException("OnListInteraction not implemented")
    }

    override fun onDetach() {
        super.onDetach()
        rvListener = null
        fabListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_person_rv, container, false)
    }

    // Initialize RecyclerView on creation
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ItemTouchHelper(simpleItemTouchCallback).attachToRecyclerView(rv_persons)
        rv_persons.layoutManager = LinearLayoutManager(context)
        rv_persons.clearOnScrollListeners()
        rv_persons.addOnScrollListener(fabBehavior!!)
        val animator = LandingAnimator()
        animator.removeDuration = 200
        rv_persons.itemAnimator = animator

        personBox = (activity!!.application as App).personBox
        val personList = personBox!!.query().order(Person_.firstName).build().find()
        val adapter = PersonAdapter()
        adapter.persons = personList
        adapter.onItemClick = {person ->
            rvListener?.onListInteraction(person)
        }
        rv_persons.adapter = adapter
    }

    fun setRVOnScrollListener(RVOnScrollListener: RecyclerView.OnScrollListener) {
        fabBehavior = RVOnScrollListener
    }
}
