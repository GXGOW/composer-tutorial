package org.gxgow.composerdemo.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_recycler_view.*
import org.gxgow.composerdemo.R
import org.gxgow.composerdemo.fragments.PersonDetailFragment
import org.gxgow.composerdemo.fragments.PersonRVFragment
import org.gxgow.composerdemo.interfaces.OnFABInteractionListener
import org.gxgow.composerdemo.interfaces.RVListListener
import org.gxgow.composerdemo.models.Person

class RecyclerViewActivity : AppCompatActivity(), OnFABInteractionListener, RVListListener {

    private val fabBehavior = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                showFAB()
            }
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0 || dy < 0 && fab.isShown) {
                hideFAB()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)
        if (supportFragmentManager.findFragmentByTag("personList") == null) {
            val fragment = PersonRVFragment()
            fragment.setRVOnScrollListener(fabBehavior)
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.activity_RV, fragment, "personList").commit()
        }
        fab.setOnClickListener { onFABInteraction()}
    }

    override fun onFABInteraction() {
        navigateToPersonDetail(null)
    }

    override fun hideFAB() {
        fab.hide()
    }

    override fun showFAB() {
        fab.show()
    }

    override fun onListInteraction(person: Person) {
        navigateToPersonDetail(person)
    }

    private fun navigateToPersonDetail(person: Person?) {
        val pdf = PersonDetailFragment()
        if(person !== null) {
            val bundle = Bundle()
            bundle.putSerializable("person", person)
            pdf.arguments = bundle
        }
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.activity_RV, pdf, "detailView")
                .addToBackStack(null)
                .commit()
    }
}
