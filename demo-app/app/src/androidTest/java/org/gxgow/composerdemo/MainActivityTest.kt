package org.gxgow.composerdemo

import android.Manifest
import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.test.InstrumentationRegistry
import android.support.test.InstrumentationRegistry.getContext
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.toPackage
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.GrantPermissionRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.widget.ImageView
import org.gxgow.composerdemo.activities.MainActivity
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.stream.Collectors


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val mainActivity = IntentsTestRule<MainActivity>(MainActivity::class.java)

    @get:Rule
    val grantCameraPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.CAMERA)

    @Test
    fun launchTest() {
        onView(withId(R.id.button1)).check(matches(isDisplayed()))
        onView(withId(R.id.button2)).check(matches(isDisplayed()))
        onView(withId(R.id.imageView)).check(matches(DrawableMatcher(R.drawable.robot)))
    }

    @Test
    fun alertDialogTest() {
        onView(withId(R.id.button1)).perform(click())
        onView(withText(R.string.dialog_title)).check(matches(isDisplayed()))
        onView(withText(R.string.dialog_text)).check(matches(isDisplayed()))
    }

    @Test
    fun toastTest() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText(R.string.message_toast)).perform(click())
        onView(withText(R.string.toast_sample)).inRoot(withDecorView(not(`is`(mainActivity.activity.window.decorView)))).check(matches(isDisplayed()))
    }

    @Test
    fun cameraImageViewTest() {
        // Some vendors use a camera app different from the stock Android camera app
        // This will be checked before executing this instrumentation test
        val pm = getContext().packageManager
        val packages = pm.getInstalledApplications(0).stream()
                .map { applicationInfo -> applicationInfo.packageName }.collect(Collectors.toList<Any>())
        // Vendor-specific camera package names
        val cameraPackages = arrayOf(
                "com.android.camera",
                "com.android.camera2",
                "com.google.android.GoogleCamera",
                "org.lineageos.snap",
                "com.huawei.camera",
                "com.sec.android.app.camera",
                "com.htc.camera",
                "com.asus.camera",
                "com.samsung.camera",
                "com.nokia.camera"
        )
        var cameraPackage = ""

        loop@ for (camera in cameraPackages) {
            if (packages.contains(camera)) {
                cameraPackage = camera
                break@loop
            }
        }

        val data = Intent()
        val image = BitmapFactory.decodeResource(
                InstrumentationRegistry.getTargetContext().resources,
                R.drawable.thoncc)
        data.putExtra("data", image)

        val resultActivity = Instrumentation.ActivityResult(Activity.RESULT_OK, data)

        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        intending(toPackage(cameraPackage)).respondWith(resultActivity)
        onView(withText(R.string.camera_open)).perform(click())
        onView(withId(R.id.imageView)).check(matches(DrawableMatcher(R.drawable.thoncc)))
    }
}

// https://stackoverflow.com/questions/29041027/android-getresources-getdrawable-deprecated-api-22
class DrawableMatcher internal constructor(private val expectedId: Int) : TypeSafeMatcher<View>(View::class.java) {
    private var resourceName: String? = null

    override fun matchesSafely(target: View): Boolean {
        if (target !is ImageView) {
            return false
        }
        if (expectedId == EMPTY) {
            return target.drawable == null
        }
        if (expectedId == ANY) {
            return target.drawable != null
        }
        val resources = target.getContext().resources
        val expectedDrawable = resources.getDrawable(expectedId, null)
        resourceName = resources.getResourceEntryName(expectedId)

        if (expectedDrawable == null) {
            return false
        }

        val bitmap = getBitmap(target.drawable)
        val otherBitmap = getBitmap(expectedDrawable)
        return bitmap.sameAs(otherBitmap)
    }

    private fun getBitmap(drawable: Drawable): Bitmap {
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        drawable.draw(canvas)
        return bitmap
    }

    override fun describeTo(description: Description) {
        description.appendText("with drawable from resource id: ")
        description.appendValue(expectedId)
        if (resourceName != null) {
            description.appendText("[")
            description.appendText(resourceName)
            description.appendText("]")
        }
    }

    companion object {
        internal val EMPTY = -1
        internal val ANY = -2
    }
}