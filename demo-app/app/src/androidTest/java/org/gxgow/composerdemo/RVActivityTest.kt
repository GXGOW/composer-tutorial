import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import org.gxgow.composerdemo.R
import org.gxgow.composerdemo.activities.RecyclerViewActivity
import org.gxgow.composerdemo.adapters.PersonAdapter
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class RVActivityTest {
    @get:Rule
    val rvActivity = ActivityTestRule<RecyclerViewActivity>(RecyclerViewActivity::class.java)


    companion object {
        private const val firstName = "Rudy"
        private const val lastName = "Kerckhove"
        private const val age = 28
        private const val phoneNumber = "0474123456"
    }

    @Test
    fun addPersonTest_firstNameOnly() {
        testRequiredInput(R.id.et_firstName, firstName, R.id.et_lastName)
    }

    @Test
    fun addPersonTest_lastNameOnly() {
        testRequiredInput(R.id.et_lastName, lastName, R.id.et_firstName)
    }

    @Test
    fun addPersonTest_ageOnly() {
        testRequiredInput(R.id.et_age, age.toString(), R.id.et_phoneNumber)
    }

    @Test
    fun addPersonTest_phoneNumberOnly() {
        testRequiredInput(R.id.et_phoneNumber, phoneNumber, R.id.et_age)
    }

    @Test
    fun editPersonTest() {
        onView(withId(R.id.rv_persons)).perform(RecyclerViewActions.actionOnItemAtPosition<PersonAdapter.PersonViewHolder>(1, click()))
        onView(withId(R.id.et_lastName))
                .perform(clearText())
                .perform(typeText("Janssens"))
                .perform(closeSoftKeyboard())
        onView(withId(R.id.btn_save)).perform(click())
        onView(withId(R.id.rv_persons)).perform(RecyclerViewActions.scrollToHolder(withHolderPersonView("Janssens")))
    }

    @Test
    fun addPersonTest() {
        onView(withId(R.id.fab)).perform(click())
        onView(withId(R.id.et_firstName)).perform(typeText(firstName))
        onView(withId(R.id.et_lastName)).perform(typeText(lastName))
        onView(withId(R.id.et_age)).perform(typeText(age.toString()))
        onView(withId(R.id.et_phoneNumber)).perform(typeText(phoneNumber)).perform(closeSoftKeyboard())
        onView(withId(R.id.btn_save)).perform(click())
        onView(withId(R.id.rv_persons)).perform(RecyclerViewActions.scrollToHolder(withHolderPersonView(firstName)))
    }

    @Test
    fun removeItemTest() {
        onView(withId(R.id.rv_persons)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, swipeRight()))
        onView(withId(R.id.rv_persons)).check(matches(not(atPosition(2, withText("Person100")))))
    }

    private fun getString(resourceId: Int): String {
        return InstrumentationRegistry.getTargetContext().getString(resourceId)
    }

    private fun testRequiredInput(inputToFillId: Int, textToType: String, inputToCheckId: Int) {
        onView(withId(R.id.fab)).perform(click())
        onView(withId(inputToFillId)).perform(typeText(textToType)).perform(closeSoftKeyboard())
        onView(withId(R.id.btn_save)).perform(click())
        onView(withId(inputToCheckId)).check(matches(hasErrorText(getString(R.string.required))))
    }
}

fun withHolderPersonView(text: String): Matcher<RecyclerView.ViewHolder> {
    return object : BoundedMatcher<RecyclerView.ViewHolder, PersonAdapter.PersonViewHolder>(PersonAdapter.PersonViewHolder::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("ViewHolder found with text: $text")
        }

        override fun matchesSafely(item: PersonAdapter.PersonViewHolder): Boolean {
            val nameViewText = item.itemView.findViewById<TextView>(R.id.tv_fullname)
            return nameViewText != null && nameViewText.text
                    .toString().contains(text)
        }
    }
}

fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
    checkNotNull(itemMatcher)
    return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder = view.findViewHolderForAdapterPosition(position) ?:
                    return false // has no item on such position
            return itemMatcher.matches(viewHolder.itemView)
        }
    }
}